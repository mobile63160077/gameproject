import 'dart:io';
import 'dart:async';
import 'package:console/console.dart';
import 'package:test/test.dart';

void main() async {
  var canvas = ConsoleCanvas(defaultSpec: PixelSpec(color: 0));

  // for (var i = 0; i < 5; i++) {
  //   canvas.setPixel(i, i, 170);
  // }
  // print("test canvas");
  //canvas.flush();

  // Console.init();
  // print('Demo of Console Features');
  // print('------------------------');
  // Console.setCrossedOut(true);
  // print('Crossed Out');
  // Console.setCrossedOut(false);
  // Console.setBold(true);
  // print('Bold');
  // Console.setBold(false);
  Console.setTextColor(1, xterm: false);

  print('Bright Red');
  Console.resetAll();
  // for (var i = 0; i < 10; i++) {
  //   Console.setTextColor(i, xterm: false);

  //   print('color');
  //   Console.resetAll();
  // }
  // print('Progress Bar');
  // var bar = ProgressBar(complete: 100);
  // bar.update(78);
  // print('${Icon.CHECKMARK} Icons');
  print("\x1B[31mHello\x1B[0m");
  print("\x1B[37;1mHello\x1B[0m");
  print("\x1B[44;1mHello\x1B[0m");
  print("\x1B[44mHello\x1B[0m");
  print("\x1b[47m\x1b[30mPlayer magical resistence -1\x1b[0m");
  //print('Bright Red');
  // print("work");

  // print("before flush");
  // canvas.flush();
  // print("  ");
  // print("after flush");
}
