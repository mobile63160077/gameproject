// ignore_for_file: file_names

import 'dart:io';

import 'package:console/console.dart';
import 'package:multiple_random_choice/multiple_random_choice.dart';

class Map {
  late String mapName;
  List<Monster> mapMonster = [];

  Map(String name) {
    mapName = name;
  }

  String getName() {
    return mapName;
  }

  Monster getMonster(int i) {
    return mapMonster[i];
  }
}

abstract class Item {
  late String itemName;

  String getItemName() {
    return itemName;
  }
}

class AtkItem extends Item {
  late int atk;
  AtkItem(String name, this.atk) {
    itemName = name;
  }

  int getAtk() {
    return atk;
  }
}

class ConsumeItem extends Item {
  late String stat;
  late int statPoint;

  ConsumeItem(String name, this.stat, this.statPoint) {
    itemName = name;
  }
  String getStat() {
    return stat;
  }

  int getStatPoint() {
    return statPoint;
  }
}

class ConditionItem extends Item {
  late String condition;
  ConditionItem(String name, String con) {
    itemName = name;
    condition = con;
  }
}

abstract class CharacterObject {
  late int hp;
  late int maxHp = hp;
  List<int> atk = [0, 0];
  List<String> action = [];
  late int def;

  int getHp() {
    return hp;
  }

  void setHp(int hp) {
    this.hp = hp;
  }

  void showHp() {
    print("Your hp is $hp");
  }

  List<int> getAtk() {
    return atk;
  }

  void setDef(int newDef) {
    def = newDef;
  }

  int getDef() {
    return def;
  }

  List<String> getAllAction() {
    return action;
  }

  String getAction(int input) {
    return action[input];
  }

  void showStat() {
    print("class: ${getClassName()}");
    print("Max hp: $hp");
    print("atk: $atk");
    print("def: $def");
  }

  void showAction() {
    print("Action:");
    for (var i = 1; i <= action.length; i++) {
      print("($i).${action[i - 1]}");
    }
  }

  void setMeleeAtk() {}
  void setRangeAtk() {}
  String getClassName() {
    return runtimeType.toString();
  }

  double getAllDmg(String act) {
    return 0;
  }
}

mixin MeleeATk {
  void meleeAtk() {}

  int getMeleeAtk() {
    return 0;
  }
}

mixin RangeAtk {
  void rangeAtk() {}
  int getRangeAtk() {
    return 0;
  }
}

class Player extends CharacterObject implements MeleeATk {
  String item = "";

  String getItem() {
    return item;
  }

  @override
  int getMeleeAtk() {
    return atk[0];
  }

  @override
  void meleeAtk() {
    print("${getClassName()} is attack with meleeAttack");
  }
}

class Fighter extends Player with RangeAtk {
  double meleeAvt = 1.5;
  Fighter() {
    hp = 30;
    maxHp = hp;
    def = 10;
    atk[0] = 10;
    atk[1] = 8;
    action = ["melee attack", "range attack", "defend", "observe", "use item"];
  }

  // Fighter.changeClass(Player player) {
  //   hp = player.getHp();
  //   def = player.getDef();
  //   atk[0] = player.getMeleeAtk();
  //   atk[1] = player.atk[1];
  //   action = player.getAllAction();
  // }

  @override
  int getRangeAtk() {
    return atk[1];
  }

  @override
  void rangeAtk() {
    print("${getClassName()} is Attack with rangeAttack");
  }

  @override
  double getAllDmg(String act) {
    double allDmg = 0;
    switch (act) {
      case "melee attack":
        allDmg = getMeleeAtk() * meleeAvt;
        break;
      case "range attack":
        allDmg = getRangeAtk() * 1;
        break;
      case "use item":
        if (item[0] == "bomb") {
          allDmg = 20;
        } else if (item[0] == "knife") {
          allDmg = 10;
        }
        break;
      default:
    }
    return allDmg;
  }
}

class Archer extends Player implements RangeAtk {
  double rangeAvt = 1.5;

  Archer() {
    hp = 25;
    maxHp = hp;
    def = 8;
    atk[0] = 10;
    atk[1] = 8;
    action = ["melee attack", "range attack", "defend", "observe", "use item"];
  }

  // Archer.changeClass(Player player) {
  //   hp = player.getHp();
  //   def = player.getDef();
  //   atk[0] = player.getMeleeAtk();
  //   atk[1] = player.atk[1];
  //   action = player.getAllAction();
  // }

  @override
  int getRangeAtk() {
    return atk[0];
  }

  @override
  void rangeAtk() {
    print("${getClassName()} is Attack with rangeAttack");
  }

  // double getRangeAvt() {
  //   return rangeAvt;
  // }

  @override
  double getAllDmg(String act) {
    double allDmg = 0;
    switch (act) {
      case "melee attack":
        allDmg = getMeleeAtk() * 1;
        break;
      case "range attack":
        allDmg = getRangeAtk() * rangeAvt;
        break;
      case "use item":
        if (item[0] == "bomb") {
          allDmg = 20;
        } else if (item[0] == "knife") {
          allDmg = 10;
        }
        break;
      default:
    }
    return allDmg;
  }
}

class Knight extends Player {
  double defAvt = 2;

  Knight() {
    hp = 40;
    maxHp = hp;
    def = 12;
    atk[0] = 10;
    action = ["melee attack", "defend", "use item"];
  }

  double getDefAvt() {
    return defAvt;
  }

  @override
  double getAllDmg(String act) {
    double allDmg = 0;
    switch (act) {
      case "melee attack":
        allDmg = getMeleeAtk() * 1;
        break;
      case "use item":
        if (item[0] == "bomb") {
          allDmg = 20;
        } else if (item[0] == "knife") {
          allDmg = 10;
        }
        break;
      default:
    }
    return allDmg;
  }
}

class Monster extends CharacterObject {
  late String mapAvt;
  late String name;

  String getMapAvt() {
    return mapAvt;
  }

  bool checkMapAvt(String mapName) {
    if (mapName == mapAvt) {
      return true;
    }
    return false;
  }

  int getRandomAction() {
    var test = {0: 0};
    if (getClassName() == "GoblinWarrior") {
      test = {0: 50, 1: 40, 2: 10};
    } else {
      test = {0: 90, 1: 10};
    }

    var random = randomMultipleWeightedChoice(test, 1, null).elementAt(0);
    print("Enemie has ${getAction(random)}");
    return random;
  }

  @override
  void showHp() {
    print("$runtimeType hp is $hp");
  }
}

class Goblin extends Monster implements MeleeATk {
  @override
  Goblin() {
    hp = 20;
    def = 5;
    atk[0] = 12;
    mapAvt = "castle hall";
    action = ["melee attack", "defend"];
  }

  Goblin.inherit(Monster mon) {
    hp = mon.getHp();
    def = mon.getDef();
    atk[0] = mon.getAtk()[0];
    mapAvt = "castle Hall";
    action = ["melee attack", "defend"];
  }

  @override
  int getMeleeAtk() {
    return atk[0];
  }

  @override
  void meleeAtk() {
    print("${getClassName()} is Attack with meleeAttack");
  }

  @override
  bool checkMapAvt(String mapName) {
    if (mapName == mapAvt) {
      hp = (hp * 1.2).round();
      def = (def * 1.2).round();
      atk[0] = (atk[0] * 1.2).round();
      return true;
    } else {
      return false;
    }
  }
}

class GoblinArcher extends Monster implements RangeAtk {
  GoblinArcher() {
    hp = 8;
    def = 4;
    atk[1] = 11;
    mapAvt = "castle garden";
    action = ["range attack", "defend"];
  }
  GoblinArcher.inherit(Monster mon) {
    hp = mon.getHp();
    def = mon.getDef();
    atk[1] = mon.getAtk()[1];
    mapAvt = "castle garden";
    action = ["range attack", "defend"];
  }

  @override
  int getRangeAtk() {
    return atk[1];
  }

  @override
  void rangeAtk() {
    print("${getClassName()} is Attack with rangeAttack");
  }

  @override
  bool checkMapAvt(String mapName) {
    if (mapName == mapAvt) {
      hp = (hp * 1.2).round();
      def = (def * 1.2).round();
      atk[1] = (atk[1] * 1.2).round();
      return true;
    } else {
      return false;
    }
  }
}

class GoblinWarrior extends Monster implements MeleeATk, RangeAtk {
  GoblinWarrior() {
    hp = 30;
    def = 8;
    atk[0] = 15;
    atk[1] = 10;
    mapAvt = "castle courtyard";
    action = ["melee attack", "range attack", "defend"];
  }

  GoblinWarrior.inherit(Monster mon) {
    hp = mon.getHp();
    def = mon.getDef();
    atk[0] = mon.getAtk()[0];
    atk[1] = mon.getAtk()[1];
    mapAvt = "castle courtyard";
    action = ["melee attack", "range attack", "defend"];
  }
  @override
  bool checkMapAvt(String mapName) {
    if (mapName == mapAvt) {
      hp = (hp * 1.2).round();
      def = (def * 1.2).round();
      atk[0] = (atk[0] * 1.2).round();
      atk[1] = (atk[1] * 1.2).round();

      return true;
    } else {
      return false;
    }
  }

  @override
  int getMeleeAtk() {
    return atk[0];
  }

  @override
  int getRangeAtk() {
    return atk[1];
  }

  @override
  void meleeAtk() {
    print("${getClassName()} is Attack with meleeAttack");
  }

  @override
  void rangeAtk() {
    print("${getClassName()} is Attack with rangeAttack");
  }
}

class GoblinKing extends Monster implements MeleeATk {
  int skillDmg = 25;
  String skillName = "heavy attack";
  GoblinKing() {
    hp = 40;
    def = 9;
    atk[0] = 20;
    action = ["melee attack", "defend", "use skill"];
  }

  @override
  int getMeleeAtk() {
    return atk[0];
  }

  @override
  void meleeAtk() {
    print("${getClassName()} is Attack with meleeAttack");
  }

  String getSkillName() {
    return skillName;
  }

  int getSkillAtk() {
    print("${getClassName()} is use skill $skillName");
    return skillDmg;
  }
}

class Game {
  int countStage = 1;
  int countEnemies = 1;
  int countTurn = 1;
  bool isDead = false;
  String currentStage = "";
  String turn = "P";
  List<Map> mapList = [];
  late Player player1;

  Game() {
    //f1 = Fighter();
    genMap();
  }

  void genMap() {
    List<String> allMap = [
      "castle hall",
      "castle hallway",
      "castle courtyard",
      "castle garden"
    ];
    Map stage1 = Map("castle gate");
    genMon(stage1);
    mapList.add(stage1);
    var prob = {0: 50, 1: 30, 2: 10, 3: 10};
    var random = randomMultipleWeightedChoice(prob, 2, null);
    for (var m in random) {
      Map map = Map(allMap[m]);
      genMon(map);
      mapList.add(map);
    }

    // Map stage2 = Map("stage 2");
    // Map stage3 = Map("stage 3");
    Map stage4 = Map("Castle throne");
    GoblinWarrior g3 = GoblinWarrior();
    GoblinKing g4 = GoblinKing();
    stage4.mapMonster.add(g4);
    mapList.add(stage4);
  }

  void genMon(Map map) {
    List<String> allMon = ["gob", "archer", "warrior"];
    var prob = {0: 50, 1: 40, 2: 10};
    for (var j = 0; j < 3; j++) {
      var i = randomMultipleWeightedChoice(prob, 1, null).elementAt(0);
      switch (i) {
        case 0:
          Goblin g = Goblin();
          g.checkMapAvt(map.mapName);
          map.mapMonster.add(g);
          break;
        case 1:
          GoblinArcher g = GoblinArcher();
          g.checkMapAvt(map.mapName);
          map.mapMonster.add(g);
          break;
        case 2:
          GoblinWarrior g = GoblinWarrior();
          g.checkMapAvt(map.mapName);
          map.mapMonster.add(g);
          break;
        default:
      }
    }
  }

  void showWelcome() {
    print(">>Welcome to GoblinSlayer<<");
  }

  String showturn() {
    if (turn == 'P') {
      print("Your turn");
      //turn = 'E';
      return 'P';
    } else if (turn == 'E') {
      print("Enemie turn");
      //int i = countEnemies - 1;
      sleep(Duration(seconds: 1));
      //mapList[0].mapMonster[i].getRandomAction();
      //turn = 'P';
      return 'E';
    }
    return "";
  }

  String askAction() {
    var choice = -1;
    print("enter action");
    while (choice < 0 || (choice > player1.action.length)) {
      player1.showAction();
      print("Input your action");
      choice = int.parse(stdin.readLineSync()!);
    }
    return player1.getAction(choice - 1);
  }

  bool askContinue() {
    int choice;
    do {
      print("Do you want to continue? [1.Yes, 2.No]");
      choice = int.parse(stdin.readLineSync()!);
      if (choice == 1) {
        return true;
      }
    } while (choice > 2 || choice < 1);

    return false;
  }

  void showStage() {
    switch (countStage) {
      case 1:
        print(">First stage<");
        break;
      case 2:
        print(">Second stage<");
        break;
      case 3:
        print(">Third stage<");
        break;
      case 4:
        print(">Boss stage<");
        break;
      default:
    }
    print("You are at the ${mapList[countStage - 1].mapName}");
  }

  void showEnemies() {
    if (countStage <= 3) {
      print("$countEnemies of 3 enemies");
      int i = countEnemies - 1;
      print(
          "You have encouter ${mapList[countStage - 1].mapMonster[i].getClassName()}");
    } else {
      print(
          "You have encouter ${mapList[countStage - 1].mapMonster[0].getClassName()}");
    }
  }

  void fightProcess() {
    //bool choice = true;
    countTurn = 1;
    turn = 'P';
    Map map = mapList[countStage - 1];
    int oldDefP = player1.getDef();
    int oldDefE = map.getMonster(countEnemies - 1).getDef();
    while (true) {
      turn = showturn();
      if (turn == 'P') {
        turn = 'E';
        player1.setDef(oldDefP);
        player1.showHp();
        int i = countEnemies - 1;
        playerActionProcess(askAction(), map.mapMonster[i]);
        if (checkDeath(map.mapMonster[i])) {
          print("Enemie is dead");
          break;
        }
      } else if (turn == 'E') {
        int i = countEnemies - 1;
        if (map.getMonster(i).getDef() == 0) {
        } else {
          map.getMonster(i).setDef(oldDefE);
        }

        //print("count $countEnemies");
        map.mapMonster[i].showHp();
        sleep(Duration(seconds: 1));
        int act = map.mapMonster[i].getRandomAction();
        String action = map.mapMonster[i].getAction(act);
        enemieActionProcess(action, map.mapMonster[i]);
        if (checkDeath(player1)) {
          print("You are dead");
          isDead = true;
          break;
        }
        turn = 'P';
      }
      countTurn++;
    }
    countEnemies++;
  }

  void playerActionProcess(String act, Monster mon) {
    int hpAfter = mon.getHp();
    int dmg = 0;
    switch (act) {
      //"melee attack", "range attack", "defend", "use item"
      case "melee attack":
        print("You attack with melee attack");
        dmg = (player1.getAllDmg(act) - mon.getDef()).round();
        break;
      case "range attack":
        print("You attack with range attack");
        dmg = (player1.getAllDmg(act) - mon.getDef()).round();
        break;
      case "defend":
        print("You have defend");
        player1.setDef((player1.getDef() * 1.5).round());
        dmg = -1;
        break;
      case "use item":
        if (player1.item == "") {
          print("You don't have item");
          turn = 'P';
          countTurn--;
          return;
        } else {
          dmg = itemAction(mon);
        }
        break;
      case "observe":
        print("You have observed enemies");
        mon.showStat();
        sleep(Duration(seconds: 5));
        dmg = -1;
        break;
    }
    if (dmg >= 1) {
      print("You deal $dmg damage to enemie.");
      hpAfter = mon.getHp() - dmg;
      if (hpAfter <= 0) {
        hpAfter = 0;
      }
      mon.setHp(hpAfter);
    } else if (dmg == 0) {
      print("You can't attack through enemie defense");
    }
  }

  void enemieActionProcess(String act, Monster mon) {
    int hpAfter = player1.getHp();
    int dmg = 0;
    switch (mon.runtimeType) {
      case Goblin:
        dmg = goblinActionProcess(act, Goblin.inherit(mon));
        break;
      case GoblinArcher:
        dmg = gobArcherActionProcess(act, GoblinArcher.inherit(mon));
        break;
      case GoblinWarrior:
        dmg = gobWarriorActionProcess(act, GoblinWarrior.inherit(mon));
        break;
      case GoblinKing:
        dmg = gobKingActionProcess(act, GoblinKing());
        break;
      default:
    }
    if (dmg == -1) {
    } else if (dmg == 0) {
      print("Enemie can't attack through your defense");
    } else {
      print("Enemie deal $dmg damage to you");
      hpAfter = player1.getHp() - dmg;
      if (hpAfter <= 0) {
        hpAfter = 0;
      }
      player1.setHp(hpAfter);
    }
  }

  int goblinActionProcess(String act, Goblin g) {
    int dmg = 0;
    switch (act) {
      case "melee attack":
        dmg = (g.getMeleeAtk() - player1.getDef()).round();
        break;
      case "defend":
        g.setDef((g.getDef() * 1.5.round()));
        return -1;
      default:
    }
    if (dmg <= 0) {
      return 0;
    }
    return dmg;
  }

  int gobArcherActionProcess(String act, GoblinArcher g) {
    int dmg = 0;
    switch (act) {
      case "range attack":
        dmg = (g.getRangeAtk() - player1.getDef()).round();
        print(dmg);
        break;
      case "defend":
        g.setDef((g.getDef() * 1.5.round()));
        return -1;
      default:
    }
    if (dmg <= 0) {
      return 0;
    }
    return dmg;
  }

  int gobWarriorActionProcess(String act, GoblinWarrior g) {
    int dmg = 0;
    switch (act) {
      case "melee attack":
        dmg = (g.getMeleeAtk() - player1.getDef()).round();
        break;
      case "range attack":
        dmg = (g.getRangeAtk() - player1.getDef()).round();
        break;
      case "defend":
        g.setDef((g.getDef() * 1.5.round()));
        return -1;
      default:
    }
    if (dmg <= 0) {
      return 0;
    }
    return dmg;
  }

  int gobKingActionProcess(String act, GoblinKing g) {
    if (countTurn == 5) {
      act = "use skill";
    }
    int dmg = 0;
    switch (act) {
      case "melee attack":
        dmg = (g.getMeleeAtk() - player1.getDef()).round();
        break;
      case "defend":
        g.setDef((g.getDef() * 1.5.round()));
        return -1;
      case "use skill":
        dmg = g.getSkillAtk() - player1.getDef();
        break;
      default:
    }
    if (dmg <= 0) {
      return 0;
    }
    return dmg;
  }

  int itemAction(Monster mon) {
    switch (player1.getItem()) {
      case "bomb":
        AtkItem b = AtkItem("Bomb", 20);
        print("You use ${b.getItemName()}");
        player1.item = "";
        return b.getAtk();
      case "knife":
        AtkItem k = AtkItem("flying knife", 10);
        print("You use ${k.getItemName()}");
        player1.item = "";
        return k.getAtk();
      case "hpPotion":
        if (player1.getHp() == player1.maxHp) {
          print("maxhp ${player1.maxHp}");
          print("Your hp is full");
          turn = 'P';
          countTurn--;
          return -1;
        }
        ConsumeItem h = ConsumeItem("Potion", "hp", 15);
        print("You use ${h.getItemName()}");
        print("Your hp +${h.statPoint}");
        int hp = player1.getHp() + h.statPoint;
        print("maxhp ${player1.maxHp}");
        if (hp > player1.maxHp) {
          hp = player1.maxHp;
        }
        player1.item = "";
        player1.setHp(hp);
        return -1;
      case "Poison":
        ConditionItem p = ConditionItem("Poison", "defence");
        print("You use ${p.getItemName()}");
        print("Your enemie's ${p.condition} is greatly decrease");
        mon.setDef(0);
        player1.item = "";
        return -1;
    }

    return 0;
  }

  int askClass() {
    int selected = 0;
    do {
      print("Please select your character class.");
      print("1.Fighter 2.Archer 3.Knight");
      selected = int.parse(stdin.readLineSync()!);
      switch (selected) {
        case 1:
          player1 = Fighter();
          break;
        case 2:
          player1 = Archer();
          break;
        case 3:
          player1 = Knight();
          break;
        default:
      }
      player1.showStat();
    } while (selected < 1 || selected > 3 || askContinue() == false);

    return selected;
  }

  bool checkDeath(CharacterObject obj) {
    if (obj.getHp() == 0) {
      sleep(Duration(seconds: 2));
      return true;
    }
    return false;
  }

  void genItem() {
    List<String> item = ["bomb", "knife", "hpPotion", "Poison"];
    var prob = {0: 30, 1: 40, 2: 20, 3: 10};
    var random = randomMultipleWeightedChoice(prob, 1, null).elementAt(0);

    print("You got an item : ${item[random]}");
    if (player1.item != "") {
      print("You already have an item");
      print("Do you want to replace item? [1.yes, 2.no]");
      var choice = int.parse(stdin.readLineSync()!);
      if (choice == 1) {
        player1.item = item[random];
      }
    } else {
      player1.item = item[random];
    }
  }
}

void main() {
  Game game = Game();
  clearScreen();
  game.showWelcome();
  game.askClass();

  // while (game.countEnemies <= 3) {
  //   if (game.countEnemies == 1) {}

  //   // game.showturn();
  //   // game.askAction();
  //   bool choice = game.askContinue();
  //   if (choice) {}
  // }
  // game.player1.item = "Poison";
  // game.countStage = 4;

  while (game.isDead == false) {
    int i = 0;
    while (
        game.countEnemies <= 3 && game.isDead == false && game.countStage < 4) {
      clearScreen();

      game.showStage();
      game.showEnemies();
      game.fightProcess();
    }
    if (game.countStage == 4) {
      clearScreen();
      game.showStage();
      game.showEnemies();
      game.fightProcess();
    }
    if (game.isDead == false) {
      print("You defeat ${game.mapList[game.countStage - 1].getName()}");
      if (game.countStage == 4) {
        clearScreen();
        print("You have conquer the Goblin castle");
        sleep(Duration(seconds: 1));
        print("Thanks for playing");
        break;
      }
      game.genItem();
      game.askContinue();
      i++;
      game.countStage++;
      game.countEnemies = 1;
    } else {
      print("GAME OVER");
      break;
    }
  }

  //game.genItem();

  //game.askContinue();
}

void clearScreen() {
  var canvas = ConsoleCanvas(defaultSpec: PixelSpec(color: 0));
  canvas.flush();
  print("  ");
}
